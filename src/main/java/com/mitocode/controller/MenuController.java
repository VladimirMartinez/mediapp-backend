package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Menu;
import com.mitocode.service.IMenuService;

@RestController
@RequestMapping("/menus")
public class MenuController {
	
	@Autowired
	private IMenuService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Menu>> listar() {
		List<Menu> menues = new ArrayList<>();
		menues = service.listar();
		return new ResponseEntity<List<Menu>>(menues, HttpStatus.OK);
	}
	
	@GetMapping(value = "/usuario/{nombre:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Menu>> listar(@PathVariable("nombre") String nombre) {
		List<Menu> menues = new ArrayList<>();
		menues = service.listarMenuPorUsuario(nombre);
		return new ResponseEntity<List<Menu>>(menues, HttpStatus.OK);
	}
	
	@GetMapping(value="/pageable", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Menu>> listarPegable(Pageable pageable){
		Page<Menu> menu= null;
		menu= service.listarPageable(pageable);
		
		return new ResponseEntity<Page<Menu>>(menu, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Menu> listarId(@PathVariable ("id") Integer idMenu){
		Menu menu = new Menu();
		menu= service.listarId(idMenu);
		
		return new ResponseEntity<Menu>(menu, HttpStatus.OK);
		
	}
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Menu menu){
		Menu men = new Menu();
		men = service.registrar(menu);
		 
		 return new ResponseEntity<Object>(men, HttpStatus.OK);
		
	}
	
	@PutMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> modificar(@RequestBody Menu menu){
		Menu men = new Menu();
		
		men = service.modificar(menu);
		
		return new ResponseEntity<Object>(men, HttpStatus.OK);
	}
	
	
	
}
