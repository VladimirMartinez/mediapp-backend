package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Rol;
import com.mitocode.service.IRolService;


@RestController
@RequestMapping("/roles")
public class RolController {
	
	@Autowired
	private IRolService service;
	
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Rol>> listar(){
		List<Rol> roles = new ArrayList<>();
		roles=service.listar();
		return new ResponseEntity<List<Rol>>(roles, HttpStatus.OK);
	}
	@GetMapping(value="/pageable", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Rol>> listarPegable(Pageable pageable){
		Page<Rol> rol= null;
		rol= service.listarPageable(pageable);
		
		return new ResponseEntity<Page<Rol>>(rol, HttpStatus.OK);
	}

	@GetMapping(value="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Rol> listarId(@PathVariable ("id") Integer idRol){
		Rol rol = new Rol();
		rol= service.listarId(idRol);
		
		return new ResponseEntity<Rol>(rol, HttpStatus.OK);
		
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Rol rol){
		Rol role = new Rol();
		role = service.registrar(rol);
		 
		 return new ResponseEntity<Object>(role, HttpStatus.OK);
		
	}
	
	@PutMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> modificar(@RequestBody Rol rol){
		Rol role = new Rol();
		role = service.modificar(rol);
		 
		 return new ResponseEntity<Object>(role, HttpStatus.OK);
		
	}
}
