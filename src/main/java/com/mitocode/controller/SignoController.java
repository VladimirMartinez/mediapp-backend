package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignoService;

@RestController
@RequestMapping("/signos")
public class SignoController {
	
	@Autowired
	private ISignoService service;
	
	
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Signos>> listar(){
		
		List<Signos> signo= new ArrayList<>();
		signo= service.listar();
		
		return new ResponseEntity<List<Signos>>(signo, HttpStatus.OK);
		
	}
	
	@GetMapping(value="/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Signos>> listarPageable(Pageable pageable){
		Page<Signos> signos= null;
		
		signos=service.listarPageable(pageable);
		
		return new ResponseEntity<Page<Signos>>(signos, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resource<Signos> listarId(@PathVariable("id") Integer id){
		
		Signos signos = new Signos();
		signos = service.listarId(id);
		if(signos==null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		Resource<Signos> resource = new Resource<Signos>(signos);
		
		return resource;
		
		
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Signos signos)
	{
		Signos sig = new Signos();
		sig = service.registrar(signos);
		return new ResponseEntity<Object>(sig, HttpStatus.OK);
	}
	
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@RequestBody Signos signos){
		
		service.modificar(signos);
		
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	
	@DeleteMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar (@PathVariable Integer id) {
		Signos sig = service.listarId(id);
		if(sig==null) {
			throw new ModeloNotFoundException("ID: "+ id);
			
		}else {
			service.eliminar(id);
		}
		
	}
	

}
