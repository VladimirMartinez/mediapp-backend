package com.mitocode.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="signo")
public class Signos {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idSigno;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime fecha;
	
	
	@Column(name="temperatura", length = 3, precision = 2)
	private double temperatura;
	
	
	@Column(name="pulso", nullable=false)
	private String pulso;
	
	@Column(name="ritmo_cardiaco", nullable=false)
	private String ritmoRespiratorio;
	
	
	@ManyToOne
	@JoinColumn(name="id_paciente", nullable=false)
	private Paciente paciente;


	public int getIdSigno() {
		return idSigno;
	}


	public void setIdSigno(int idSigno) {
		this.idSigno = idSigno;
	}


	public LocalDateTime getFecha() {
		return fecha;
	}


	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}


	public double getTemperatura() {
		return temperatura;
	}


	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}


	public String getPulso() {
		return pulso;
	}


	public void setPulso(String pulso) {
		this.pulso = pulso;
	}


	public String getRitmoRespiratorio() {
		return ritmoRespiratorio;
	}


	public void setRitmoRespiratorio(String ritmoRespiratorio) {
		this.ritmoRespiratorio = ritmoRespiratorio;
	}


	public Paciente getPaciente() {
		return paciente;
	}


	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	
	

}
