package com.mitocode.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.dto.MenuDTO;
import com.mitocode.model.Menu;

public interface IMenuService {
	
	
	
	
	Menu registrar(Menu menu);

	Menu modificar(Menu menu);

	void eliminar(int idMenu);

	Menu listarId(int idMenu);

	List<Menu> listar();
	
	List<Menu> listarMenuPorUsuario(String nombre);
	
	Page<Menu> listarPageable(Pageable pageable);
	
}
