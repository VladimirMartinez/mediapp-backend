package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignoDAO;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignoService;


@Service
public class SignoServiceImpl implements ISignoService{
	
	
	@Autowired
	private ISignoDAO dao;

	@Override
	public Signos registrar(Signos signo) {
		// TODO Auto-generated method stub
		return dao.save(signo);
	}

	@Override
	public Signos modificar(Signos signo) {
		// TODO Auto-generated method stub
		return dao.save(signo);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
		dao.delete(id);
		
	}

	@Override
	public Signos listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Signos> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Page<Signos> listarPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return dao.findAll(pageable);
	}

}
