package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
	
	@Autowired
	private IUsuarioDAO dao;

	@Override
	public Usuario registrar(Usuario usuario) {
		// TODO Auto-generated method stub
		return dao.save(usuario);
	}

	@Override
	public Usuario modificar(Usuario usuario) {
		// TODO Auto-generated method stub
		return dao.save(usuario);
	}

	@Override
	public void eliminar(int idUsuario) {
		dao.delete(idUsuario);
		
	}

	@Override
	public Usuario listarId(int idUsuario) {
		// TODO Auto-generated method stub
		return dao.findOne(idUsuario);
	}

	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Page<Usuario> listarPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return dao.findAll(pageable);
	}

}
