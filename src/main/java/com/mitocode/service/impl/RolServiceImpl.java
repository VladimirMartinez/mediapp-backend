package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IRolDAO;
import com.mitocode.model.Rol;
import com.mitocode.service.IRolService;

@Service
public class RolServiceImpl implements IRolService {
	
	@Autowired
	private IRolDAO dao;

	@Override
	public Rol registrar(Rol rol) {
		// TODO Auto-generated method stub
		return dao.save(rol) ;
	}

	@Override
	public Rol modificar(Rol rol) {
		// TODO Auto-generated method stub
		return dao.save(rol);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Rol listarId(int idRol) {
		// TODO Auto-generated method stub
		return dao.findOne(idRol);
	}

	@Override
	public List<Rol> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Page<Rol> listarPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return dao.findAll(pageable);
	}

}
