package com.mitocode.dto;

import java.util.List;

import com.mitocode.model.Menu;
import com.mitocode.model.Usuario;

public class MenuDTO {
	private Usuario usuario;
	private List<Menu> menu;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Menu> getMenu() {
		return menu;
	}

	public void setMenu(List<Menu> menu) {
		this.menu = menu;
	}

}
